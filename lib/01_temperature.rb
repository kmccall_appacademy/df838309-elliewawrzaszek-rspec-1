def ftoc(temp_f)
  (temp_f - 32) * 5/9
end

def ctof(temp_c)
    temp_c.to_f * 9.0/5.0 + 32.0
end
