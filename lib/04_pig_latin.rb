def translate(string)
  result = []
  array = string.split
  array.each do |word|
    result << latinize(word)
  end
  result.join(" ")
end

def latinize(word)
  vowels = "aeio"
  until vowels.include?(word[0].downcase)
    word = word[1..-1] + word[0]
  end
  word + "ay"
end
