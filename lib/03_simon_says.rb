def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, n = 2)
  [string] * n * ' '
end

def start_of_word(word, n)
  word[0...n]
end

def first_word(string)
  string.split[0]
end

def titleize(string)
  little_words = ["the", "it", "and", "of", "is", "are", "over"]
  array = string.split
  result = []
  array.each do |word|
    if little_words.include?(word)
      result << word
    else
      result << word.capitalize
    end
  end
  title = []
  title << result[0].capitalize
  title << result[1..-1]
  title.join(" ").strip
end
